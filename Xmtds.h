/*
** Xmtds.h for Xmtds
** 
** Made by Hydrix
** Login   <hydrix@epita.fr>
** 
** Started on  Mon Dec  5 22:57:09 1994 hydrix
** Last update Mon Dec  5 23:00:45 1994 hydrix
*/

#ifndef __XMTDS_H__
#define __XMTDS_H__	1

#define XmtdsCBackground	"Background"
#define XmtdsCBlockHeight	"BlockHeight"
#define XmtdsCBlockWidth	"BlockWidth"
#define XmtdsCCallback		"Callback"
#define XmtdsCColorList		"ColorList"
#define XmtdsCComm		"Comm"
#define XmtdsCDisplayName	"DisplayName"
#define XmtdsCFd		"Fd"
#define XmtdsCForeground	"Foreground"
#define XmtdsCGameHeight	"GameHeight"
#define XmtdsCGameWidth		"GameWidth"
#define XmtdsCInterval		"Interval"
#define XmtdsCPixHeight		"PixHeight"
#define XmtdsCPixWidth		"PixWidth"
#define XmtdsCProgram		"Program"
#define XmtdsCRasterHeight	"RasterHeight"
#define XmtdsCRasterWidth	"RasterWidth"
#define XmtdsCShadowColor	"ShadowColor"
#define XmtdsCSlave		"Slave"
#define XmtdsCUserData		"UserData"

#define XmtdsNbackground	"background"
#define XmtdsNblockHeight	"blockHeight"
#define XmtdsNblockWidth	"blockWidth"
#define XmtdsNcolorList		"colorList"
#define XmtdsNcomm		"comm"
#define XmtdsNdisplayName	"displayName"
#define XmtdsNfd		"fd"
#define XmtdsNforeground	"foreground"
#define XmtdsNgameHeight	"gameHeight"
#define XmtdsNgameWidth		"gameWidth"
#define XmtdsNinterval		"interval"
#define XmtdsNpixHeight		"pixHeight"
#define XmtdsNpixWidth		"pixWidth"
#define XmtdsNprogram		"program"
#define XmtdsNrasterHeight	"rasterHeight"
#define XmtdsNrasterWidth	"rasterWidth"
#define XmtdsNscoreCallback	"scoreCallback"
#define XmtdsNshadowColor	"shadowColor"
#define XmtdsNslave		"slave"
#define XmtdsNuserData		"userData"

#endif /* __XMTDS_H__ */
