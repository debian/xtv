/*
** Grab.h for XmtdsGrab widget in .
** 
** Made by MaxTheDogSays (dubray_f@epita.fr && rancur_v@epita.fr
** Login   <vr@epita.fr>
** 
** Started on  Mon May 16 22:17:04 1994 vr
** Last update Tue Nov 29 01:22:05 1994 hydrix
*/

#ifndef _XMTDSGRAB_H_
#define _XMTDSGRAB_H_	1

#include <X11/StringDefs.h>
#include "Xmtds.h"

extern WidgetClass			xmtdsGrabWidgetClass;

typedef struct _XmtdsGrabClassRec	*XmtdsGrabWidgetClass;
typedef struct _XmtdsGrabRec		*XmtdsGrabWidget;

#endif /* _XMTDSGRAB_H_ */
