/*
** XmtdsI.h for Xmtds
**
** Made by MaxTheDogSays (dubray_f@epita.fr && rancur_v@epita.fr)
** Login   <mtds@epita.fr>
**
** Started on  Thu Jan 12 23:27:08 1995 mtds
** Last update Thu Jan 12 23:37:46 1995 mtds
*/

#ifndef __XMTDSI_H__
#define __XMTDSI_H__	1

#include <stdio.h>
#include <errno.h>

void	*malloc();
void	*realloc();

#define EXIT_FAIL		exit(255)
#define EXIT_SUCCEED		exit(0)

#define BUF_SIZE		512

#define SYSCALL_ERROR(syscall)	{perror(syscall);fprintf(stderr,"%s %d\n",\
							 __FILE__,__LINE__);\
				    EXIT_FAIL;}

#define MALLOC(ptr,size,comm)	{if (((ptr) = malloc(size)) == NULL)\
				 {perror("malloc");\
				     printf("File %s Line %d : %s\n",\
					    __FILE__,__LINE__,comm);\
				     EXIT_FAIL;}}

#define REALLOC(ptr,size,comm)	{if (((ptr) = realloc((ptr),(size))) == NULL)\
				 {perror("realloc");\
				     printf("File %s Line %d : %s\n",\
					    __FILE__,__LINE__,comm);\
				     EXIT_FAIL;}}
/* Xlib */

#define LOOKUP_STRING_BUFSIZ	512

#define XFS_WIDTH(xfs)		(((xfs)->max_bounds).width)
#define XFS_HEIGHT(xfs)		((xfs)->descent+(xfs)->ascent)
#define XFS_DESCENT(xfs)	((xfs)->descent)
#define XFS_FID(xfs)		((xfs)->fid)

/* Xtoolkit */

#define W_SCREEN(w)		DefaultScreen(XtDisplay(w))
#define W_DEPTH(w)		DefaultDepth(XtDisplay(w),W_SCREEN(w))
#define W_BPX(w)		BlackPixel(XtDisplay(w),W_SCREEN(w))
#define W_WPX(w)		WhitePixel(XtDisplay(w),W_SCREEN(w))
#define W_ROOT(w)		DefaultRootWindow(XtDisplay(w))
#define W_CMAP(w)		DefaultColormap(XtDisplay(w),W_SCREEN(w))

/* algeb */

#define ABS(x)			(((x) < 0)?-(x):(x))

#ifndef MAX
#define MAX(x,y)	((x)>(y)?(x):(y))
#endif /* MAX */

#ifndef MIN
#define MIN(x,y)	((x)<(y)?(x):(y))
#endif /* MIN */

#endif /* __XMTDSI_H__ */
